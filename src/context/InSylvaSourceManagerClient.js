class InSylvaSourceManagerClient {

    async post(method, path, requestContent) {

        const headers = {
            "Content-Type": "application/json",
            //"Access-Control-Allow-Origin": "*"
            "Authorization": `Bearer ${this.token}`
        }
        const response = await fetch(`${this.baseUrl}${path}`, {
            method: method,
            headers,
            body: JSON.stringify(requestContent),
            mode: 'cors'
        });

        const responseContent = await response.json();

        return responseContent;
    }

    async publicFields() {
        const path = `/publicFieldList`
        const result = this.post('GET', `${path}`);
        return result;
    };

    async userPolicyFields(userId) {
        const path = `/policy-stdfields`
        const result = this.post('POST', `${path}`, { userId });
        return result;
    };

    async sourcesWithIndexes(kc_id) {
        const path = `/source_indexes`
        const result = this.post('POST', `${path}`, { kc_id });
        return result;
    };

}

InSylvaSourceManagerClient.prototype.baseUrl = null;
InSylvaSourceManagerClient.prototype.token = null;
export { InSylvaSourceManagerClient }
