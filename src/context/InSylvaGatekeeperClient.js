class InSylvaGatekeeperClient {

    async post(method, path, requestContent) {

        const headers = {
            "Content-Type": "application/json",
            //"Access-Control-Allow-Origin": "*"
            "Authorization": `Bearer ${this.token}`
        }
        const response = await fetch(`${this.baseUrl}${path}`, {
            method: method,
            headers,
            body: JSON.stringify(requestContent),
            mode: 'cors'
        });
        const responseContent = await response.json();
        return responseContent;
    }

    async getGroups() {
        const path = `/user/groups`
        const groups = await this.post('POST', `${path}`, {})
        return groups
    }

    async getUserRequests(kcId) {
        const path = `/user/list-requests-by-user`;
        const userRequests = await this.post('POST', `${path}`, {
            kcId,
        });
        return userRequests;
    }

    async createUserRequest(kcId, message) {
        const path = `/user/create-request`;
        const userRequests = await this.post('POST', `${path}`, {
            kcId,
            message
        });
        return userRequests;
    }

    async deleteUserRequest(id) {
        console.log("client")
        const path = `/user/delete-request`;
        const userRequests = await this.post('DELETE', `${path}`, {
            id
        });
    }

    async findRole() {
        const path = `/role/find`;
        const roles = await this.post('GET', `${path}`);
        return roles;
    }

    async kcId({ email }) {
        const path = `/user/kcid`;

        const kcId = await this.post('POST', `${path}`, {
            email
        });

        return kcId;
    }

    async sendMail(subject, message) {
        const path = `/user/send-mail`;

        await this.post('POST', `${path}`, {
            subject,
            message
        });
    }

    async findOneUser(id) {
        const path = `/user/findOne`;
        const user = await this.post('POST', `${path}`, {
            id,
        });
        return user;
    }

    async findOneUserWithGroupAndRole(id) {
        const path = `/user/one-with-groups-and-roles`;
        const user = await this.post('POST', `${path}`, {
            id,
        });
        return user;
    }

    async getUserDetails(kcId) {
        const path = `/user/detail`;
        const userDetails = await this.post('GET', `${path}`, {
            kcId,
        });
        return userDetails;
    }

    async addUserHistory(kcId, query, name, uiStructure, description) {
        const path = `/user/add-history`;
        await this.post('POST', `${path}`, {
            kcId,
            query,
            name,
            uiStructure,
            description
        });
    }

    async userHistory(kcId) {
        const path = `/user/fetch-history`;
        const history = await this.post('POST', `${path}`, {
            kcId
        });
        return history
    }

    async deleteUserHistory(id) {
        const path = `/user/delete-history`;
        await this.post('POST', `${path}`, {
            id
        });
    }

}

InSylvaGatekeeperClient.prototype.baseUrl = null;
InSylvaGatekeeperClient.prototype.token = null;
export { InSylvaGatekeeperClient }