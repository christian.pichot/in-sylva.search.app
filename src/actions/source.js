import { InSylvaSourceManagerClient } from '../context/InSylvaSourceManagerClient'
import { InSylvaSearchClient } from '../context/InSylvaSearchClient'
import { refreshToken } from "../context/UserContext";
import { tokenTimedOut } from '../Utils'

const ismClient = new InSylvaSourceManagerClient();
ismClient.baseUrl = process.env.REACT_APP_IN_SYLVA_SOURCE_MANAGER_PORT ? `${process.env.REACT_APP_IN_SYLVA_SOURCE_MANAGER_HOST}:${process.env.REACT_APP_IN_SYLVA_SOURCE_MANAGER_PORT}` : `${window._env_.REACT_APP_IN_SYLVA_SOURCE_MANAGER_HOST}`
const isClient = new InSylvaSearchClient();
isClient.baseUrl = process.env.REACT_APP_IN_SYLVA_SEARCH_PORT ? `${process.env.REACT_APP_IN_SYLVA_SEARCH_HOST}:${process.env.REACT_APP_IN_SYLVA_SEARCH_PORT}` : `${window._env_.REACT_APP_IN_SYLVA_SEARCH_HOST}`

export { fetchPublicFields, fetchUserPolicyFields, fetchSources, searchQuery, getQueryCount };

async function fetchPublicFields() {
    if (tokenTimedOut(process.env.REACT_KEYCLOAK_TOKEN_VALIDITY)) {
        refreshToken()
    }
    ismClient.token = sessionStorage.getItem("access_token")
    const result = await ismClient.publicFields()
    // if (result) {
    return (result)
    // } else {
    // return (null)
    // }
}

async function fetchUserPolicyFields(kcId) {
    if (tokenTimedOut(process.env.REACT_KEYCLOAK_TOKEN_VALIDITY)) {
        refreshToken()
    }
    ismClient.token = sessionStorage.getItem("access_token")
    const result = await ismClient.userPolicyFields(kcId)
    return (result)
}

async function fetchSources(kcId) {
    if (tokenTimedOut(process.env.REACT_KEYCLOAK_TOKEN_VALIDITY)) {
        refreshToken()
    }
    ismClient.token = sessionStorage.getItem("access_token")
    const result = await ismClient.sourcesWithIndexes(kcId)
    return (result)
}

async function searchQuery(query) {
    if (tokenTimedOut(process.env.REACT_KEYCLOAK_TOKEN_VALIDITY)) {
        refreshToken()
    }
    isClient.token = sessionStorage.getItem("access_token")

    const result = await isClient.search(query)
    return (result)
}

async function getQueryCount(queries) {
    if (tokenTimedOut(process.env.REACT_KEYCLOAK_TOKEN_VALIDITY)) {
        refreshToken()
    }
    isClient.token = sessionStorage.getItem("access_token")

    const result = await isClient.count(queries)
    return (result)
}