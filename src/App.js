/* eslint-disable jsx-a11y/alt-text */
import React from 'react';

import {
  EuiPage,
  EuiPageBody,
} from '@elastic/eui';

import { HashRouter, Route, Switch, Redirect } from "react-router-dom";
import Layout from './components/Layout'

// import { useUserState, useUserDispatch, checkUserLogin } from "./context/UserContext";
// import { redirect } from "./Utils.js";

/* const loginUrl = (process.env.REACT_APP_IN_SYLVA_LOGIN_PORT) ? `${process.env.REACT_APP_IN_SYLVA_LOGIN_HOST}:${process.env.REACT_APP_IN_SYLVA_LOGIN_PORT}` : `${window._env_.REACT_APP_IN_SYLVA_LOGIN_HOST}:${window.env.REACT_APP_IN_SYLVA_LOGIN_PORT}`

debugger

console.log(loginUrl) */

const App = (props) => {

  /* var { isAuthenticated } = useUserState()
  var userDispatch = useUserDispatch() */

  /* useEffect(() => {
    checkUserLogin(userDispatch, props.userId, props.accessToken);
    redirect(!isAuthenticated, loginUrl + '?requestType=search')
  }, []) */

  return (

    < EuiPage restrictWidth={false} >
      <EuiPageBody>
        <HashRouter>
          <Switch>
            <Route exact path="/" render={() => <Redirect to="/app/home" />} />
            <Route component={Layout} />
            <Route component={Error} />
          </Switch>
        </HashRouter>

      </EuiPageBody>
    </EuiPage >
  );

}

export default App;

